﻿namespace WebApplication8.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Double Price { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
