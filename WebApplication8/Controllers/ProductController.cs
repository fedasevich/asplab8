﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication8.Models;

namespace WebApplication8.Controllers
{
    public class ProductController : Controller
    {
        public IActionResult Index()
        {
            List<Product> products = new List<Product>
        {
            new Product { Id = 1, Name = "Product 1", Price = 10.99, CreatedDate = DateTime.Now },
            new Product { Id = 2, Name = "Product 2", Price = 20.49, CreatedDate = DateTime.Now },
            new Product { Id = 3, Name = "Product 3", Price = 5.99, CreatedDate = DateTime.Now }
        };

            return View(products);
        }
    }
}
